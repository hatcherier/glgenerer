# Pseudo graphical editor that generates code
#### This frankenstein I'm using for lab. So code is not for humans, at the time.

You can use only several OpenGL shapes:

- LINES
- QUADS
- TRIANGLE_FAN
- POLYGON (unlimited vertexes)

## Getting started

### How to build?

- Install on your machine Java 8 execution environment (jre1.8.0_241)
- Add JARs to your project (**gluegen-rt.jar** and **jogl-all.jar** in **jogamp** directory)
- build project using Java 8 execution environment

### How to use?

Once you've run the project, you can use **LEFT/RIGHT arrows** to choose the shape you want to generate. The chosen shape you can see in console output. If you want to terminate drawing the polygon, you should press **ESCAPE**. Generated code you can find in **shapes** directory.