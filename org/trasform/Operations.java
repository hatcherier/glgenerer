package org.trasform;

import java.util.Vector;

public class Operations {
	// Matrix-vector product 
	public static Vector<Integer> MVP (Matrix matrix,
			Vector<Integer> initCoords) {
		Vector<Integer> prod = new Vector<Integer>();
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				prod.add((int) (matrix.m[i][j] * initCoords.get(j)));
			}
		}
		return prod;
	}
}
