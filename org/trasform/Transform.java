package org.trasform;

import java.util.Vector;

public class Transform {
	Matrix m;
	int x, y, z;
	
	Transform(int _x, int _y, int _z) {
		x = _x;
		y = _y;
		z = _z;
	}
	
	Vector<Integer> translate(Vector<Integer> initCoords) {
		m.translateInit(x, y, z);
		return Operations.MVP(m, initCoords);
	}
	
	Vector<Integer> scale(Vector<Integer> initCoords) {
		m.scaleInit(x, y, z);
		return Operations.MVP(m, initCoords);
	}
	
	Vector<Integer> rotateX(Vector<Integer> initCoords, int angle) {
		m.rotateXInit(angle);
		return Operations.MVP(m, initCoords);
	}
	
	Vector<Integer> rotateY(Vector<Integer> initCoords, int angle) {
		m.rotateYInit(angle);
		return Operations.MVP(m, initCoords);
	}
	
	Vector<Integer> rotateZ(Vector<Integer> initCoords, int angle) {
		m.rotateZInit(angle);
		return Operations.MVP(m, initCoords);
	}
}
