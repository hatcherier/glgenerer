package org.trasform;

public class Matrix {
	float m[][] = {
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0}};
	
	void translateInit(int x, int y, int z) {
		for (int i = 0; i < 4; i++) {
			m[i][i] = 1;
		}
		m[0][3] = x;
		m[1][3] = y;
		m[2][3] = z;
	}
	
	void scaleInit(int x, int y, int z) {
		m[0][0] = x;
		m[1][1] = y;
		m[2][2] = z;
		m[3][3] = 1;
	}
	
	void rotateXInit(int angle) {
		m[0][0] = 1;
		m[1][1] = (float) Math.cos(angle);
		m[1][2] = (float) -Math.sin(angle);
		m[2][1] = (float) Math.sin(angle);
		m[2][2] = (float) Math.cos(angle);
		m[3][3] = 1;
	}
	
	void rotateYInit(int angle) {
		m[0][0] = (float) Math.cos(angle);
		m[0][2] = (float) Math.sin(angle);
		m[1][1] = 1;
		m[2][0] = (float) -Math.sin(angle);
		m[2][2] = (float) Math.cos(angle);
		m[3][3] = 1;
	}
	
	void rotateZInit(int angle) {
		m[0][0] = (float) Math.cos(angle);
		m[0][1] = (float) -Math.sin(angle);
		m[1][0] = (float) Math.sin(angle);
		m[1][1] = (float) Math.cos(angle);
		m[2][2] = 1;
		m[3][3] = 1;
	}
}
