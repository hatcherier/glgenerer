package org.fio;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.engine.DrawEngine;
import org.input.KeyInput;

public class BufferWriter {
	
	public static void coordBufferedWritter (int x, int y) throws IOException
	{
		String fileNameFormat = DrawEngine.shapes.get(DrawEngine.activeShape)
				+ "_" + 0 + ".cobj";
	     
	    BufferedWriter writer = new BufferedWriter(new FileWriter("shapes/"
	    		+ fileNameFormat, true));
	    
	    // Calling methods for writing
	    if (DrawEngine.activeShape != 3) {
	    	if (DrawEngine.isDrawing == false) {
	    		writeBegin(writer);
	    		DrawEngine.isDrawing = true;
	    	}
	    	if (DrawEngine.vertexCount > 0) {
	    		writeVertex2f(writer, x, y);
	    		DrawEngine.vertexCount--;
	    	}
	    	else {
	    		writeEnd(writer);
	    		DrawEngine.isDrawing = false;
	    		DrawEngine.vertexInit();
	    		KeyInput.printNewShape();
	    		DrawEngine.shapeCount++;
	    	}
	    }
	    
	    if (DrawEngine.activeShape == 3) {
	    	if (DrawEngine.isDrawing == false) {
	    		writeBegin(writer);
	    		DrawEngine.isDrawing = true;
	    	}
	    	if (DrawEngine.vertexCount > 0) {
	    		writeVertex2f(writer, x, y);
	    	}
	    	if (DrawEngine.vertexCount == 0) {
	    		writeEnd(writer);
	    	}
	    }
	    
	    writer.close();
	}
	
	public static void writeBegin (BufferedWriter w) throws IOException {
		w.write("gl.glBegin(GL2.GL_" +
				DrawEngine.shapes.get(DrawEngine.activeShape) + ");\n");
	}
	
	public static void writeVertex2f (BufferedWriter w, int x, int y) throws IOException {
		w.write("gl.glVertex2f(" + x + ", " + y + ");\n");
	}
	
	public static void writeEnd (BufferedWriter w) throws IOException {
		w.write("gl.glEnd();\n\n");
	}
}
