package org.engine;

import java.util.ArrayList;
import java.util.List;

public class DrawEngine {
	
	public static List<String> shapes = new ArrayList<String>();
	public static int activeShape = 0;
	public static int shapeCount = 0;
	public static int vertexCount = 2;
	public static boolean isDrawing = false;
	
	public static void shapeInit () {
		shapes.add("LINES");
		shapes.add("QUADS");
		shapes.add("TRIANGLE_FAN");
		shapes.add("POLYGON");
	}
	
	public static void vertexInit () {
    	if (activeShape == shapes.indexOf("LINES")) {
    		vertexCount = 2;
    	}
    	
    	if (activeShape == shapes.indexOf("QUADS")) {
    		vertexCount = 4;
    	}
    	
    	if (activeShape == shapes.indexOf("TRIANGLE_FAN")) {
    		vertexCount = 3;
    	}
    	
    	if (activeShape == shapes.indexOf("POLYGON")) {
    		vertexCount = 3;
    	}
	}
	
}
