package org.input;

import java.io.IOException;

import org.engine.DrawEngine;
import org.fio.BufferWriter;

import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.KeyListener;

public class KeyInput implements KeyListener {
	
	@Override
	public void keyPressed (KeyEvent arg0) {
		// TODO Auto-generated method stub
		int keyCode = arg0.getKeyCode();
	    switch(keyCode) {
	        case KeyEvent.VK_LEFT:
	            // handle left
	        	if (DrawEngine.isDrawing == true) {
	        		printIsDrawingRestriction();
	        		break;
	        	}
	        	if (DrawEngine.activeShape == 0)
	        		DrawEngine.activeShape = 3;
	        	else DrawEngine.activeShape--;
	        	DrawEngine.vertexInit();
	        	printNewShape();
	            break;
	        case KeyEvent.VK_RIGHT:
	            // handle right
	        	if (DrawEngine.isDrawing == true) {
	        		printIsDrawingRestriction();
	        		break;
	        	}
	        	if (DrawEngine.activeShape == 3)
	        		DrawEngine.activeShape = 0;
	        	else DrawEngine.activeShape++;
	        	DrawEngine.vertexInit();
	        	printNewShape();
	            break;
	        case KeyEvent.VK_ESCAPE:
	        	if (DrawEngine.activeShape == 3) {
	        		if (DrawEngine.isDrawing == true) {
	        			DrawEngine.vertexCount = 0;
	        			try {
	        				BufferWriter.coordBufferedWritter(0, 0);
	        			} catch (IOException e) {
	        				// TODO Auto-generated catch block
	        				e.printStackTrace();
	        			}
	        		DrawEngine.isDrawing = false;
	        		}
	        	}
	        	break;
	     }
	}

	@Override
	public void keyReleased (KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public static void printNewShape () {
		System.out.println("Creating "
				+ DrawEngine.shapes.get(DrawEngine.activeShape));
	}
	
	public void printIsDrawingRestriction () {
		System.out.println("At the moment is drawing"
				+ " an object. Please terminate it"
				+ " by creating the rest of vertexes");
	}
}
