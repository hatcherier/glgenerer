package org.graphics;

import org.input.KeyInput;
import org.input.MouseInput;

import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.FPSAnimator;

public class UserView {
	
	private static GLWindow window = null;
	
	public static int screenWidth = 800;
	public static int screenHeight = 600;
	
	public static float unitsWide = 1;
	
	public static void init () {
		
		GLProfile.initSingleton();
		GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities caps = new GLCapabilities(profile);
		
		window = GLWindow.create(caps);
		window.setSize(screenWidth, screenHeight);
		window.setRealized(false);
		window.addGLEventListener(new EventListener());
		window.setTitle("Tarantino forever");
		window.setResizable(false);
		window.addMouseListener(new MouseInput());
		window.addKeyListener(new KeyInput());
		
		FPSAnimator animator = new FPSAnimator(window, 60);
		animator.start();
		
		window.setVisible(true);
		//window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		window.requestFocus();
	}
	
	public static int getWindowWidth () {
		return window.getWidth();
	}
	
	public static int getWindowHeight () {
		return window.getHeight();
	}
	
	public static void main (String[] args) {
		init();
	}
}
