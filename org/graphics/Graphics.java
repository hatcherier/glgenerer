package org.graphics;

import com.jogamp.opengl.GL2;

public class Graphics {
	
	public static void word() {
		GL2 gl = EventListener.gl;
		
		// lines
		gl.glColor3d(0.772, 0.525, 0.043);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(139, 205);
		gl.glVertex2f(147, 206);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(147, 206);
		gl.glVertex2f(154, 207);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(154, 207);
		gl.glVertex2f(161, 214);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(161, 214);
		gl.glVertex2f(164, 222);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(164, 222);
		gl.glVertex2f(165, 231);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(165, 231);
		gl.glVertex2f(166, 243);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(166, 243);
		gl.glVertex2f(166, 265);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(166, 265);
		gl.glVertex2f(162, 268);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(162, 268);
		gl.glVertex2f(166, 274);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(166, 274);
		gl.glVertex2f(155, 282);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(155, 282);
		gl.glVertex2f(147, 277);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(147, 277);
		gl.glVertex2f(143, 277);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(143, 277);
		gl.glVertex2f(139, 277);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(139, 277);
		gl.glVertex2f(130, 276);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(130, 276);
		gl.glVertex2f(125, 272);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(125, 272);
		gl.glVertex2f(120, 264);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(120, 264);
		gl.glVertex2f(118, 256);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(118, 256);
		gl.glVertex2f(117, 250);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(117, 250);
		gl.glVertex2f(117, 245);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(117, 245);
		gl.glVertex2f(118, 216);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(118, 216);
		gl.glVertex2f(120, 211);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(120, 211);
		gl.glVertex2f(123, 209);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(123, 209);
		gl.glVertex2f(128, 206);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(128, 206);
		gl.glVertex2f(135, 205);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(135, 205);
		gl.glVertex2f(141, 204);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(138, 224);
		gl.glVertex2f(136, 260);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(136, 260);
		gl.glVertex2f(138, 261);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(138, 261);
		gl.glVertex2f(141, 259);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(141, 259);
		gl.glVertex2f(143, 257);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(143, 257);
		gl.glVertex2f(144, 223);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(144, 223);
		gl.glVertex2f(141, 222);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(141, 222);
		gl.glVertex2f(138, 224);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(171, 207);
		gl.glVertex2f(168, 221);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(168, 221);
		gl.glVertex2f(172, 220);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(172, 220);
		gl.glVertex2f(172, 265);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(173, 264);
		gl.glVertex2f(175, 267);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(175, 267);
		gl.glVertex2f(177, 270);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(177, 270);
		gl.glVertex2f(181, 271);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(181, 271);
		gl.glVertex2f(186, 273);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(186, 274);
		gl.glVertex2f(191, 274);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(191, 274);
		gl.glVertex2f(195, 275);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(196, 275);
		gl.glVertex2f(200, 274);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(200, 274);
		gl.glVertex2f(208, 272);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(208, 272);
		gl.glVertex2f(211, 268);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(211, 268);
		gl.glVertex2f(212, 264);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(212, 264);
		gl.glVertex2f(216, 256);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(216, 256);
		gl.glVertex2f(217, 251);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(217, 251);
		gl.glVertex2f(216, 242);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(216, 242);
		gl.glVertex2f(216, 238);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(216, 238);
		gl.glVertex2f(215, 228);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(215, 228);
		gl.glVertex2f(216, 225);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(216, 225);
		gl.glVertex2f(216, 220);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(216, 220);
		gl.glVertex2f(218, 219);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(218, 219);
		gl.glVertex2f(220, 206);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(220, 206);
		gl.glVertex2f(196, 205);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(196, 205);
		gl.glVertex2f(197, 219);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(197, 219);
		gl.glVertex2f(199, 219);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(199, 219);
		gl.glVertex2f(199, 257);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(199, 257);
		gl.glVertex2f(198, 259);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(198, 259);
		gl.glVertex2f(194, 259);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(194, 259);
		gl.glVertex2f(192, 257);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(192, 257);
		gl.glVertex2f(190, 220);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(190, 220);
		gl.glVertex2f(193, 219);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(193, 219);
		gl.glVertex2f(191, 207);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(191, 207);
		gl.glVertex2f(171, 206);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(225, 206);
		gl.glVertex2f(225, 219);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(225, 219);
		gl.glVertex2f(227, 219);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(227, 219);
		gl.glVertex2f(228, 264);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(228, 264);
		gl.glVertex2f(224, 264);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(224, 264);
		gl.glVertex2f(224, 277);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(224, 277);
		gl.glVertex2f(271, 278);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(271, 278);
		gl.glVertex2f(272, 253);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(272, 253);
		gl.glVertex2f(272, 253);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(272, 253);
		gl.glVertex2f(254, 252);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(254, 252);
		gl.glVertex2f(254, 259);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(254, 259);
		gl.glVertex2f(247, 259);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(247, 259);
		gl.glVertex2f(245, 248);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(245, 248);
		gl.glVertex2f(257, 247);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(257, 247);
		gl.glVertex2f(256, 236);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(256, 236);
		gl.glVertex2f(247, 236);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(247, 236);
		gl.glVertex2f(247, 219);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(247, 219);
		gl.glVertex2f(251, 219);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(251, 219);
		gl.glVertex2f(255, 229);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(255, 229);
		gl.glVertex2f(271, 228);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(271, 228);
		gl.glVertex2f(269, 206);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(269, 206);
		gl.glVertex2f(224, 205);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(277, 207);
		gl.glVertex2f(277, 207);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(277, 207);
		gl.glVertex2f(276, 219);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(276, 219);
		gl.glVertex2f(278, 220);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(278, 220);
		gl.glVertex2f(277, 260);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(277, 260);
		gl.glVertex2f(276, 260);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(276, 260);
		gl.glVertex2f(277, 276);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(278, 276);
		gl.glVertex2f(294, 275);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(294, 275);
		gl.glVertex2f(294, 250);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(294, 250);
		gl.glVertex2f(304, 273);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(304, 273);
		gl.glVertex2f(323, 274);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(323, 274);
		gl.glVertex2f(322, 220);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(322, 220);
		gl.glVertex2f(324, 220);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(324, 220);
		gl.glVertex2f(324, 207);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(324, 207);
		gl.glVertex2f(308, 207);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(308, 207);
		gl.glVertex2f(308, 232);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(308, 232);
		gl.glVertex2f(295, 206);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(295, 206);
		gl.glVertex2f(277, 206);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(331, 206);
		gl.glVertex2f(331, 206);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(384, 205);
		gl.glVertex2f(384, 205);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(384, 238);
		gl.glVertex2f(384, 238);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(372, 237);
		gl.glVertex2f(372, 237);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(370, 221);
		gl.glVertex2f(370, 221);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(369, 221);
		gl.glVertex2f(369, 221);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(367, 262);
		gl.glVertex2f(367, 262);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(369, 262);
		gl.glVertex2f(369, 262);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(370, 274);
		gl.glVertex2f(370, 274);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(343, 273);
		gl.glVertex2f(343, 273);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(343, 264);
		gl.glVertex2f(343, 264);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(345, 264);
		gl.glVertex2f(345, 264);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(347, 224);
		gl.glVertex2f(347, 224);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(345, 224);
		gl.glVertex2f(345, 224);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(345, 236);
		gl.glVertex2f(345, 236);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(332, 236);
		gl.glVertex2f(332, 236);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(331, 206);
		gl.glVertex2f(331, 206);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(389, 206);
		gl.glVertex2f(389, 276);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(389, 276);
		gl.glVertex2f(412, 274);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(412, 274);
		gl.glVertex2f(410, 206);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(410, 206);
		gl.glVertex2f(391, 205);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(469, 205);
		gl.glVertex2f(466, 275);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(466, 275);
		gl.glVertex2f(447, 274);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(447, 274);
		gl.glVertex2f(432, 247);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(432, 247);
		gl.glVertex2f(434, 278);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(434, 278);
		gl.glVertex2f(417, 275);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(417, 275);
		gl.glVertex2f(417, 205);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(417, 205);
		gl.glVertex2f(436, 205);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(436, 205);
		gl.glVertex2f(448, 233);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(448, 233);
		gl.glVertex2f(447, 206);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(447, 206);
		gl.glVertex2f(466, 205);
		gl.glEnd();
		
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(333, 208);
		gl.glVertex2f(385, 209);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(385, 209);
		gl.glVertex2f(386, 239);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(386, 239);
		gl.glVertex2f(375, 239);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(375, 239);
		gl.glVertex2f(370, 226);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(370, 226);
		gl.glVertex2f(370, 276);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(370, 276);
		gl.glVertex2f(347, 276);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(347, 276);
		gl.glVertex2f(349, 224);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(349, 224);
		gl.glVertex2f(346, 239);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(346, 239);
		gl.glVertex2f(334, 238);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(334, 238);
		gl.glVertex2f(333, 209);
		gl.glEnd();
	}
	
	public static void tree() {
		GL2 gl = EventListener.gl;
		
		// color
		gl.glColor3d(0.333, 0.420, 0.184);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(615, 22);
		gl.glVertex2f(603, 29);
		gl.glVertex2f(595, 48);
		gl.glVertex2f(604, 66);
		gl.glVertex2f(610, 79);
		gl.glVertex2f(640, 77);
		gl.glVertex2f(663, 64);
		gl.glVertex2f(672, 41);
		gl.glVertex2f(663, 25);
		gl.glVertex2f(641, 23);
		gl.glVertex2f(617, 19);
		gl.glVertex2f(601, 29);
		gl.glEnd();
		
		gl.glColor3d(0.545, 0.271, 0.075);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(631, 78);
		gl.glVertex2f(631, 125);
		gl.glVertex2f(635, 125);
		gl.glVertex2f(636, 77);
		gl.glEnd();
		
		// lines
		gl.glColor3f(0,  0,  0);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(637, 125);
		gl.glVertex2f(637, 78);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(637, 78);
		gl.glVertex2f(640, 75);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(640, 75);
		gl.glVertex2f(642, 74);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(642, 74);
		gl.glVertex2f(648, 74);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(648, 74);
		gl.glVertex2f(649, 74);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(649, 74);
		gl.glVertex2f(650, 73);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(650, 73);
		gl.glVertex2f(652, 71);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(652, 71);
		gl.glVertex2f(657, 72);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(657, 72);
		gl.glVertex2f(659, 71);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(659, 71);
		gl.glVertex2f(659, 71);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(659, 71);
		gl.glVertex2f(659, 68);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(659, 68);
		gl.glVertex2f(662, 67);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(662, 67);
		gl.glVertex2f(662, 64);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(662, 64);
		gl.glVertex2f(664, 61);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(664, 61);
		gl.glVertex2f(666, 59);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(666, 59);
		gl.glVertex2f(668, 60);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(668, 60);
		gl.glVertex2f(671, 60);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(671, 60);
		gl.glVertex2f(671, 58);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(671, 58);
		gl.glVertex2f(668, 55);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(668, 55);
		gl.glVertex2f(668, 54);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(668, 54);
		gl.glVertex2f(670, 50);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(670, 50);
		gl.glVertex2f(670, 46);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(670, 46);
		gl.glVertex2f(669, 45);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(669, 45);
		gl.glVertex2f(666, 45);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(666, 45);
		gl.glVertex2f(666, 39);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(666, 39);
		gl.glVertex2f(666, 37);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(666, 37);
		gl.glVertex2f(664, 36);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(664, 36);
		gl.glVertex2f(663, 35);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(663, 35);
		gl.glVertex2f(661, 35);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(661, 35);
		gl.glVertex2f(661, 34);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(661, 34);
		gl.glVertex2f(662, 32);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(662, 32);
		gl.glVertex2f(661, 29);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(661, 29);
		gl.glVertex2f(658, 28);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(658, 28);
		gl.glVertex2f(656, 26);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(656, 26);
		gl.glVertex2f(654, 25);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(654, 25);
		gl.glVertex2f(651, 22);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(651, 22);
		gl.glVertex2f(649, 23);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(649, 23);
		gl.glVertex2f(646, 28);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(646, 28);
		gl.glVertex2f(644, 28);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(644, 28);
		gl.glVertex2f(641, 26);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(641, 26);
		gl.glVertex2f(640, 25);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(640, 25);
		gl.glVertex2f(636, 26);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(636, 26);
		gl.glVertex2f(634, 25);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(634, 25);
		gl.glVertex2f(633, 24);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(632, 23);
		gl.glVertex2f(631, 24);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(631, 24);
		gl.glVertex2f(629, 25);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(629, 25);
		gl.glVertex2f(625, 22);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(625, 22);
		gl.glVertex2f(624, 25);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(624, 25);
		gl.glVertex2f(621, 25);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(621, 25);
		gl.glVertex2f(621, 24);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(621, 24);
		gl.glVertex2f(619, 24);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(619, 24);
		gl.glVertex2f(619, 25);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(619, 25);
		gl.glVertex2f(617, 26);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(617, 26);
		gl.glVertex2f(615, 25);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(615, 25);
		gl.glVertex2f(616, 27);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(616, 27);
		gl.glVertex2f(615, 28);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(615, 28);
		gl.glVertex2f(612, 27);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(612, 27);
		gl.glVertex2f(612, 30);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(612, 30);
		gl.glVertex2f(610, 31);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(610, 31);
		gl.glVertex2f(606, 32);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(606, 32);
		gl.glVertex2f(606, 35);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(606, 35);
		gl.glVertex2f(604, 35);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(604, 35);
		gl.glVertex2f(604, 36);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(604, 36);
		gl.glVertex2f(604, 37);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(604, 37);
		gl.glVertex2f(603, 38);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(603, 38);
		gl.glVertex2f(601, 39);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(601, 39);
		gl.glVertex2f(602, 41);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(602, 41);
		gl.glVertex2f(601, 41);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(601, 41);
		gl.glVertex2f(598, 42);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(598, 42);
		gl.glVertex2f(600, 44);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(600, 44);
		gl.glVertex2f(598, 46);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(598, 46);
		gl.glVertex2f(598, 47);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(598, 47);
		gl.glVertex2f(599, 48);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(599, 48);
		gl.glVertex2f(598, 50);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(598, 50);
		gl.glVertex2f(600, 52);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(600, 52);
		gl.glVertex2f(598, 55);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(598, 55);
		gl.glVertex2f(598, 56);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(598, 56);
		gl.glVertex2f(599, 58);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(599, 58);
		gl.glVertex2f(602, 60);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(602, 60);
		gl.glVertex2f(599, 61);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(599, 61);
		gl.glVertex2f(599, 63);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(599, 63);
		gl.glVertex2f(602, 66);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(602, 66);
		gl.glVertex2f(601, 67);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(601, 67);
		gl.glVertex2f(601, 70);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(601, 70);
		gl.glVertex2f(602, 71);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(602, 71);
		gl.glVertex2f(604, 71);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(604, 71);
		gl.glVertex2f(606, 71);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(606, 71);
		gl.glVertex2f(608, 71);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(608, 71);
		gl.glVertex2f(610, 71);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(610, 71);
		gl.glVertex2f(610, 74);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(610, 74);
		gl.glVertex2f(609, 75);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(609, 75);
		gl.glVertex2f(609, 78);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(609, 77);
		gl.glVertex2f(612, 77);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(612, 77);
		gl.glVertex2f(617, 76);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(617, 76);
		gl.glVertex2f(620, 75);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(620, 75);
		gl.glVertex2f(622, 74);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(622, 74);
		gl.glVertex2f(623, 74);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(623, 74);
		gl.glVertex2f(624, 76);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(624, 76);
		gl.glVertex2f(626, 76);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(626, 76);
		gl.glVertex2f(628, 77);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(628, 77);
		gl.glVertex2f(632, 78);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(632, 78);
		gl.glVertex2f(635, 78);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(635, 78);
		gl.glVertex2f(632, 77);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(632, 77);
		gl.glVertex2f(632, 82);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(632, 82);
		gl.glVertex2f(631, 124);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(631, 124);
		gl.glVertex2f(636, 125);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(636, 125);
		gl.glVertex2f(636, 125);
		gl.glEnd();


	}
	
	public static void art () {
		
		GL2 gl = EventListener.gl;
		
		// drum
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(797, 327);
		gl.glVertex2f(499, 136);
		gl.glEnd();
		
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(286, 133);
		gl.glVertex2f(1, 317);
		gl.glEnd();
		
		//tineoglinda jos backround
		gl.glColor3f(0.502f, 0.502f, 0.502f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(398, 180);
		gl.glVertex2f(395, 203);
		gl.glVertex2f(405, 206);
		gl.glVertex2f(409, 179);
		gl.glEnd();
		
		// tine oglinda sus
		gl.glColor3f(0.502f, 0.502f, 0.502f);
		gl.glBegin(GL2.GL_TRIANGLE_FAN);
		gl.glVertex2f(384, 173);
		gl.glVertex2f(401, 189);
		gl.glVertex2f(418, 173);
		gl.glEnd();
		
		//tine oglinda jos
		gl.glColor3f(0.439f, 0.502f, 0.565f);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(402, 188);
		gl.glVertex2f(400, 202);
		gl.glEnd();

		// oglinda top
		gl.glColor3f(0.184f, 0.310f, 0.310f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(372, 199);
		gl.glVertex2f(422, 200);
		gl.glVertex2f(432, 208);
		gl.glVertex2f(435, 221);
		gl.glVertex2f(432, 226);
		gl.glVertex2f(373, 223);
		gl.glVertex2f(365, 220);
		gl.glVertex2f(362, 209);
		gl.glEnd();
		
		//oglinda top whitepart
		gl.glColor3f(0.827f, 0.827f, 0.827f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(375, 200);
		gl.glVertex2f(367, 205);
		gl.glVertex2f(387, 205);
		gl.glVertex2f(404, 207);
		gl.glVertex2f(424, 207);
		gl.glVertex2f(417, 202);
		gl.glVertex2f(395, 201);
		gl.glVertex2f(377, 199);
		gl.glVertex2f(369, 202);
		gl.glEnd();
		
		// oglinda left
		gl.glColor3f(0.184f, 0.310f, 0.310f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(115, 306);
		gl.glVertex2f(106, 304);
		gl.glVertex2f(87, 305);
		gl.glVertex2f(62, 299);
		gl.glVertex2f(49, 290);
		gl.glVertex2f(47, 279);
		gl.glVertex2f(52, 266);
		gl.glVertex2f(61, 262);
		gl.glVertex2f(67, 261);
		gl.glVertex2f(80, 264);
		gl.glVertex2f(105, 271);
		gl.glVertex2f(112, 280);
		gl.glVertex2f(116, 289);
		gl.glVertex2f(123, 300);
		gl.glVertex2f(116, 307);
		gl.glEnd();
		
		// carcasa left 1 white
		gl.glColor3f(0.753f, 0.753f, 0.753f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(3, 399);
		gl.glVertex2f(120, 303);
		gl.glVertex2f(127, 305);
		gl.glVertex2f(8, 404);
		gl.glEnd();
		
		//carcasa 2 left black
		gl.glColor3f(0, 0, 0);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(126, 308);
		gl.glVertex2f(19, 398);
		gl.glEnd();
		
		//carcasa top left 1
		gl.glColor3f(0.663f, 0.663f, 0.663f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(121, 304);
		gl.glVertex2f(126, 296);
		gl.glVertex2f(148, 191);
		gl.glVertex2f(158, 197);
		gl.glVertex2f(151, 288);
		gl.glVertex2f(127, 308);
		gl.glEnd();
		
		//carcasa top left 2
		gl.glColor3f(0.827f, 0.827f, 0.827f);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(148, 211);
		gl.glVertex2f(141, 283);
		gl.glEnd();

		//carcasa top left 3
		gl.glColor3f(0.827f, 0.827f, 0.827f);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(148, 211);
		gl.glVertex2f(141, 283);
		gl.glEnd();
		
		// carcasa left
		gl.glColor3f(0.753f, 0.753f, 0.753f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(4, 401);
		gl.glVertex2f(4, 412);
		gl.glVertex2f(68, 417);
		gl.glVertex2f(68, 391);
		gl.glVertex2f(129, 339);
		gl.glVertex2f(130, 309);
		gl.glEnd();

		// carcasa right
		gl.glColor3f(0.753f, 0.753f, 0.753f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(781, 406);
		gl.glVertex2f(672, 311);
		gl.glVertex2f(650, 308);
		gl.glVertex2f(748, 403);
		gl.glEnd();


		// carcasa right 1 write
		gl.glColor3f(0.753f, 0.753f, 0.753f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(797, 400);
		gl.glVertex2f(670, 295);
		gl.glVertex2f(668, 299);
		gl.glVertex2f(786, 404);
		gl.glEnd();
		
		//carcasa right 2 black
		gl.glColor3f(0, 0, 0);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(784, 405);
		gl.glVertex2f(669, 301);
		gl.glEnd();

		//carcasa top right 1
		gl.glColor3f(0.412f, 0.412f, 0.412f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(672, 297);
		gl.glVertex2f(651, 191);
		gl.glVertex2f(630, 207);
		gl.glVertex2f(640, 280);
		gl.glVertex2f(667, 300);
		gl.glEnd();
		
		// carcasa top right 2
		gl.glColor3f(0.663f, 0.663f, 0.663f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(633, 207);
		gl.glVertex2f(639, 215);
		gl.glVertex2f(644, 240);
		gl.glVertex2f(646, 273);
		gl.glVertex2f(647, 283);
		gl.glVertex2f(642, 280);
		gl.glVertex2f(630, 211);
		gl.glEnd();
		
		// carcasa top 0
		gl.glColor3f(0.663f, 0.663f, 0.663f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(377, 155);
		gl.glVertex2f(433, 155);
		gl.glVertex2f(419, 176);
		gl.glVertex2f(383, 175);
		gl.glEnd();

		// carcasa top 1
		gl.glColor3f(0.663f, 0.663f, 0.663f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(629, 206);
		gl.glVertex2f(618, 198);
		gl.glVertex2f(521, 180);
		gl.glVertex2f(416, 175);
		gl.glVertex2f(417, 153);
		gl.glVertex2f(517, 159);
		gl.glVertex2f(601, 171);
		gl.glVertex2f(651, 196);
		gl.glEnd();
		
		// carcasa top 2
		gl.glColor3f(0.663f, 0.663f, 0.663f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(388, 175);
		gl.glVertex2f(279, 177);
		gl.glVertex2f(157, 201);
		gl.glVertex2f(149, 195);
		gl.glVertex2f(169, 179);
		gl.glVertex2f(233, 165);
		gl.glVertex2f(358, 153);
		gl.glVertex2f(386, 153);
		gl.glVertex2f(387, 174);
		gl.glEnd();

		// carcasa down 1
		gl.glColor3f(0.663f, 0.663f, 0.663f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(643, 271);
		gl.glVertex2f(631, 280);
		gl.glVertex2f(418, 240);
		gl.glVertex2f(152, 270);
		gl.glVertex2f(149, 289);
		gl.glVertex2f(429, 281);
		gl.glVertex2f(626, 300);
		gl.glVertex2f(663, 295);
		gl.glVertex2f(643, 280);
		gl.glEnd();
		
		// carcasa down 2
		gl.glColor3f(0.412f, 0.412f, 0.412f);
		gl.glBegin(GL2.GL_TRIANGLE_FAN);
		gl.glVertex2f(352, 251);
		gl.glVertex2f(417, 244);
		gl.glVertex2f(456, 251);
		gl.glEnd();

		// carcasa down 3
		gl.glColor3f(0.412f, 0.412f, 0.412f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(639, 272);
		gl.glVertex2f(636, 280);
		gl.glVertex2f(511, 260);
		gl.glVertex2f(289, 257);
		gl.glVertex2f(269, 269);
		gl.glVertex2f(440, 267);
		gl.glVertex2f(551, 278);
		gl.glVertex2f(622, 290);
		gl.glVertex2f(640, 290);
		gl.glVertex2f(646, 279);
		gl.glEnd();
		
		// box 1
		gl.glColor3f(0.663f, 0.663f, 0.663f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(480, 303);
		gl.glVertex2f(547, 305);
		gl.glVertex2f(612, 315);
		gl.glVertex2f(616, 328);
		gl.glVertex2f(613, 345);
		gl.glVertex2f(483, 341);
		gl.glVertex2f(477, 337);
		gl.glVertex2f(477, 306);
		gl.glEnd();
		
		// box 2
		gl.glColor3f(0.412f, 0.412f, 0.412f);
		gl.glBegin(GL2.GL_QUADS);
		gl.glVertex2f(534, 312);
		gl.glVertex2f(535, 317);
		gl.glVertex2f(544, 317);
		gl.glVertex2f(544, 311);
		gl.glEnd();
		
		// lines
		gl.glColor3f(0, 0, 0);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(379, 213);
		gl.glVertex2f(414, 213);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(414, 213);
		gl.glVertex2f(390, 218);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(390, 218);
		gl.glVertex2f(410, 221);
		gl.glEnd();
		
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(6, 398);
		gl.glVertex2f(116, 309);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(116, 309);
		gl.glVertex2f(123, 300);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(123, 300);
		gl.glVertex2f(135, 251);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(135, 251);
		gl.glVertex2f(140, 227);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(140, 227);
		gl.glVertex2f(142, 206);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(142, 206);
		gl.glVertex2f(148, 193);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(148, 193);
		gl.glVertex2f(159, 186);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(159, 186);
		gl.glVertex2f(170, 180);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(170, 180);
		gl.glVertex2f(196, 174);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(196, 174);
		gl.glVertex2f(225, 166);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(225, 166);
		gl.glVertex2f(251, 165);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(251, 165);
		gl.glVertex2f(255, 163);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(235, 166);
		gl.glVertex2f(235, 166);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(235, 166);
		gl.glVertex2f(273, 162);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(273, 162);
		gl.glVertex2f(304, 160);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(304, 160);
		gl.glVertex2f(334, 156);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(335, 157);
		gl.glVertex2f(361, 154);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(361, 154);
		gl.glVertex2f(385, 154);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(385, 154);
		gl.glVertex2f(412, 155);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(412, 155);
		gl.glVertex2f(436, 154);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(436, 154);
		gl.glVertex2f(467, 157);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(467, 157);
		gl.glVertex2f(498, 160);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(498, 160);
		gl.glVertex2f(534, 162);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(534, 162);
		gl.glVertex2f(576, 170);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(576, 170);
		gl.glVertex2f(612, 177);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(612, 177);
		gl.glVertex2f(640, 188);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(640, 188);
		gl.glVertex2f(650, 194);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(650, 193);
		gl.glVertex2f(659, 218);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(659, 218);
		gl.glVertex2f(664, 247);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(664, 247);
		gl.glVertex2f(668, 277);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(668, 277);
		gl.glVertex2f(669, 297);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(669, 297);
		gl.glVertex2f(693, 318);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(693, 318);
		gl.glVertex2f(723, 344);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(723, 344);
		gl.glVertex2f(774, 386);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(774, 386);
		gl.glVertex2f(797, 402);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(609, 206);
		gl.glVertex2f(565, 196);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(565, 196);
		gl.glVertex2f(516, 191);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(516, 191);
		gl.glVertex2f(481, 183);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(481, 183);
		gl.glVertex2f(453, 182);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(453, 182);
		gl.glVertex2f(433, 178);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(433, 178);
		gl.glVertex2f(429, 173);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(429, 173);
		gl.glVertex2f(435, 168);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(435, 168);
		gl.glVertex2f(448, 165);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(448, 165);
		gl.glVertex2f(480, 165);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(480, 165);
		gl.glVertex2f(506, 170);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(506, 170);
		gl.glVertex2f(536, 174);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(536, 174);
		gl.glVertex2f(563, 181);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(563, 181);
		gl.glVertex2f(586, 189);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(585, 188);
		gl.glVertex2f(606, 194);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(606, 194);
		gl.glVertex2f(609, 202);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(615, 199);
		gl.glVertex2f(632, 209);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(632, 209);
		gl.glVertex2f(636, 225);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(636, 225);
		gl.glVertex2f(640, 246);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(640, 246);
		gl.glVertex2f(642, 266);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(642, 266);
		gl.glVertex2f(640, 282);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(640, 282);
		gl.glVertex2f(620, 281);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(620, 281);
		gl.glVertex2f(597, 275);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(597, 275);
		gl.glVertex2f(569, 270);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(569, 270);
		gl.glVertex2f(539, 265);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(539, 265);
		gl.glVertex2f(514, 260);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(514, 260);
		gl.glVertex2f(480, 250);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(480, 250);
		gl.glVertex2f(453, 247);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(453, 247);
		gl.glVertex2f(428, 242);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(428, 242);
		gl.glVertex2f(418, 241);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(418, 241);
		gl.glVertex2f(386, 245);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(386, 245);
		gl.glVertex2f(358, 247);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(358, 247);
		gl.glVertex2f(326, 253);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(326, 253);
		gl.glVertex2f(306, 253);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(306, 253);
		gl.glVertex2f(283, 254);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(283, 254);
		gl.glVertex2f(277, 255);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(429, 175);
		gl.glVertex2f(418, 175);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(418, 175);
		gl.glVertex2f(407, 168);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(407, 168);
		gl.glVertex2f(396, 168);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(396, 168);
		gl.glVertex2f(384, 175);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(384, 175);
		gl.glVertex2f(360, 180);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(360, 180);
		gl.glVertex2f(335, 185);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(335, 185);
		gl.glVertex2f(311, 189);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(311, 189);
		gl.glVertex2f(276, 194);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(276, 194);
		gl.glVertex2f(255, 198);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(255, 198);
		gl.glVertex2f(241, 193);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(379, 174);
		gl.glVertex2f(369, 170);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(369, 170);
		gl.glVertex2f(350, 172);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(350, 172);
		gl.glVertex2f(331, 173);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(331, 173);
		gl.glVertex2f(315, 173);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(315, 173);
		gl.glVertex2f(284, 174);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(284, 174);
		gl.glVertex2f(255, 178);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(255, 178);
		gl.glVertex2f(239, 183);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(122, 302);
		gl.glVertex2f(118, 295);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(118, 295);
		gl.glVertex2f(113, 292);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(113, 292);
		gl.glVertex2f(111, 280);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(111, 280);
		gl.glVertex2f(100, 273);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(100, 273);
		gl.glVertex2f(82, 268);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(82, 268);
		gl.glVertex2f(67, 263);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(67, 263);
		gl.glVertex2f(56, 266);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(56, 266);
		gl.glVertex2f(52, 269);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(52, 269);
		gl.glVertex2f(49, 277);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(49, 277);
		gl.glVertex2f(50, 286);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(50, 286);
		gl.glVertex2f(54, 296);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(54, 296);
		gl.glVertex2f(65, 301);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(65, 301);
		gl.glVertex2f(75, 304);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(75, 304);
		gl.glVertex2f(86, 308);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(86, 308);
		gl.glVertex2f(97, 307);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(97, 307);
		gl.glVertex2f(108, 305);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(108, 305);
		gl.glVertex2f(118, 309);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(117, 309);
		gl.glVertex2f(122, 303);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(83, 274);
		gl.glVertex2f(102, 287);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(102, 287);
		gl.glVertex2f(65, 273);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(65, 273);
		gl.glVertex2f(86, 298);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(86, 298);
		gl.glVertex2f(61, 290);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(419, 176);
		gl.glVertex2f(409, 184);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(409, 184);
		gl.glVertex2f(411, 200);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(411, 200);
		gl.glVertex2f(421, 203);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(421, 203);
		gl.glVertex2f(429, 207);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(429, 207);
		gl.glVertex2f(434, 217);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(434, 217);
		gl.glVertex2f(435, 224);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(435, 224);
		gl.glVertex2f(433, 229);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(433, 229);
		gl.glVertex2f(422, 227);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(422, 227);
		gl.glVertex2f(408, 227);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(408, 227);
		gl.glVertex2f(397, 227);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(397, 227);
		gl.glVertex2f(385, 227);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(385, 227);
		gl.glVertex2f(373, 224);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(373, 224);
		gl.glVertex2f(365, 219);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(365, 219);
		gl.glVertex2f(363, 209);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(363, 209);
		gl.glVertex2f(367, 204);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(367, 204);
		gl.glVertex2f(376, 201);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(376, 201);
		gl.glVertex2f(389, 201);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(389, 201);
		gl.glVertex2f(395, 202);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(395, 202);
		gl.glVertex2f(396, 189);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(396, 189);
		gl.glVertex2f(386, 177);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(386, 177);
		gl.glVertex2f(387, 174);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(387, 174);
		gl.glVertex2f(389, 172);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(389, 172);
		gl.glVertex2f(393, 171);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(393, 171);
		gl.glVertex2f(396, 169);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(396, 169);
		gl.glVertex2f(402, 169);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(402, 169);
		gl.glVertex2f(408, 169);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(408, 169);
		gl.glVertex2f(415, 172);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(415, 172);
		gl.glVertex2f(417, 176);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(417, 176);
		gl.glVertex2f(417, 176);
		gl.glEnd();
		
		// beatrix	
		gl.glColor3f(0, 0, 0);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(212, 179);
		gl.glVertex2f(205, 181);
		gl.glEnd();
		
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(219, 182);
		gl.glVertex2f(225, 183);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(225, 183);
		gl.glVertex2f(236, 186);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(236, 186);
		gl.glVertex2f(241, 193);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(241, 193);
		gl.glVertex2f(250, 199);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(250, 199);
		gl.glVertex2f(257, 203);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(257, 203);
		gl.glVertex2f(263, 207);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(263, 207);
		gl.glVertex2f(255, 207);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(255, 207);
		gl.glVertex2f(260, 215);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(260, 215);
		gl.glVertex2f(271, 225);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(271, 225);
		gl.glVertex2f(282, 229);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(282, 229);
		gl.glVertex2f(291, 234);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(291, 239);
		gl.glVertex2f(291, 239);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(287, 242);
		gl.glVertex2f(287, 242);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(268, 233);
		gl.glVertex2f(268, 233);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(276, 247);
		gl.glVertex2f(282, 252);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(282, 252);
		gl.glVertex2f(277, 259);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(277, 259);
		gl.glVertex2f(274, 261);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(163, 349);
		gl.glVertex2f(168, 358);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(168, 358);
		gl.glVertex2f(176, 371);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(176, 371);
		gl.glVertex2f(179, 370);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(179, 370);
		gl.glVertex2f(180, 362);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(180, 362);
		gl.glVertex2f(178, 352);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(178, 352);
		gl.glVertex2f(180, 352);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(180, 352);
		gl.glVertex2f(182, 357);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(182, 357);
		gl.glVertex2f(184, 363);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(184, 363);
		gl.glVertex2f(186, 371);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(186, 371);
		gl.glVertex2f(187, 367);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(187, 367);
		gl.glVertex2f(187, 358);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(187, 358);
		gl.glVertex2f(184, 351);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(330, 298);
		gl.glVertex2f(322, 291);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(320, 288);
		gl.glVertex2f(320, 288);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(320, 288);
		gl.glVertex2f(315, 282);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(315, 282);
		gl.glVertex2f(306, 277);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(306, 277);
		gl.glVertex2f(301, 274);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(301, 274);
		gl.glVertex2f(290, 270);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(290, 270);
		gl.glVertex2f(284, 268);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(284, 268);
		gl.glVertex2f(278, 268);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(278, 268);
		gl.glVertex2f(275, 268);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(275, 268);
		gl.glVertex2f(275, 268);
		gl.glEnd();
		
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(290, 237);
		gl.glVertex2f(288, 244);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(288, 244);
		gl.glVertex2f(273, 238);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(273, 238);
		gl.glVertex2f(264, 231);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(264, 231);
		gl.glVertex2f(269, 243);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(269, 243);
		gl.glVertex2f(279, 250);
		gl.glEnd();
		
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(291, 236);
		gl.glVertex2f(285, 234);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(285, 234);
		gl.glVertex2f(273, 230);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(273, 230);
		gl.glVertex2f(279, 235);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(279, 235);
		gl.glVertex2f(290, 243);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(178, 372);
		gl.glVertex2f(173, 354);
		gl.glEnd();
		
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(205, 181);
		gl.glVertex2f(198, 183);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(198, 183);
		gl.glVertex2f(194, 186);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(194, 186);
		gl.glVertex2f(190, 188);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(190, 188);
		gl.glVertex2f(188, 188);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(188, 188);
		gl.glVertex2f(183, 189);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(183, 189);
		gl.glVertex2f(180, 194);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(180, 194);
		gl.glVertex2f(176, 196);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(176, 196);
		gl.glVertex2f(169, 202);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(169, 202);
		gl.glVertex2f(164, 213);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(164, 213);
		gl.glVertex2f(158, 219);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(158, 219);
		gl.glVertex2f(156, 223);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(156, 223);
		gl.glVertex2f(154, 231);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(154, 231);
		gl.glVertex2f(154, 242);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(154, 242);
		gl.glVertex2f(154, 255);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(154, 255);
		gl.glVertex2f(152, 259);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(152, 259);
		gl.glVertex2f(152, 261);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(152, 261);
		gl.glVertex2f(150, 265);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(150, 265);
		gl.glVertex2f(151, 269);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(151, 269);
		gl.glVertex2f(149, 270);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(149, 270);
		gl.glVertex2f(147, 276);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(147, 276);
		gl.glVertex2f(146, 281);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(146, 281);
		gl.glVertex2f(147, 288);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(147, 288);
		gl.glVertex2f(147, 294);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(147, 294);
		gl.glVertex2f(150, 298);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(150, 298);
		gl.glVertex2f(153, 303);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(153, 303);
		gl.glVertex2f(153, 313);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(153, 313);
		gl.glVertex2f(153, 320);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(153, 320);
		gl.glVertex2f(152, 327);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(152, 327);
		gl.glVertex2f(154, 336);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(154, 336);
		gl.glVertex2f(162, 346);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(162, 346);
		gl.glVertex2f(155, 350);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(155, 350);
		gl.glVertex2f(150, 345);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(150, 345);
		gl.glVertex2f(142, 342);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(142, 342);
		gl.glVertex2f(129, 343);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(129, 343);
		gl.glVertex2f(123, 346);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(123, 346);
		gl.glVertex2f(114, 350);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(114, 350);
		gl.glVertex2f(109, 356);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(109, 356);
		gl.glVertex2f(104, 360);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(103, 360);
		gl.glVertex2f(102, 374);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(102, 374);
		gl.glVertex2f(98, 378);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(98, 378);
		gl.glVertex2f(101, 387);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(100, 387);
		gl.glVertex2f(98, 397);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(98, 397);
		gl.glVertex2f(100, 405);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(212, 180);
		gl.glVertex2f(217, 180);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(217, 180);
		gl.glVertex2f(221, 181);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(225, 183);
		gl.glVertex2f(225, 183);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(232, 184);
		gl.glVertex2f(232, 184);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(238, 185);
		gl.glVertex2f(238, 185);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(242, 191);
		gl.glVertex2f(242, 191);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(241, 191);
		gl.glVertex2f(241, 191);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(245, 196);
		gl.glVertex2f(245, 196);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(250, 200);
		gl.glVertex2f(250, 200);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(260, 203);
		gl.glVertex2f(260, 203);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(254, 204);
		gl.glVertex2f(254, 204);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(253, 205);
		gl.glVertex2f(253, 205);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(257, 210);
		gl.glVertex2f(257, 210);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(261, 218);
		gl.glVertex2f(261, 218);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(269, 224);
		gl.glVertex2f(269, 224);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(277, 226);
		gl.glVertex2f(277, 226);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(282, 226);
		gl.glVertex2f(282, 226);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(290, 226);
		gl.glVertex2f(290, 226);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(290, 229);
		gl.glVertex2f(290, 229);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(285, 230);
		gl.glVertex2f(285, 230);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(290, 237);
		gl.glVertex2f(290, 237);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(285, 239);
		gl.glVertex2f(285, 239);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(266, 228);
		gl.glVertex2f(266, 228);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(267, 232);
		gl.glVertex2f(267, 232);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(269, 235);
		gl.glVertex2f(269, 235);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(271, 244);
		gl.glVertex2f(271, 244);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(277, 250);
		gl.glVertex2f(277, 250);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(279, 253);
		gl.glVertex2f(279, 253);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(274, 254);
		gl.glVertex2f(274, 254);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(274, 256);
		gl.glVertex2f(274, 256);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(273, 261);
		gl.glVertex2f(273, 261);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(273, 261);
		gl.glVertex2f(273, 271);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(273, 271);
		gl.glVertex2f(275, 280);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(275, 280);
		gl.glVertex2f(286, 289);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(286, 289);
		gl.glVertex2f(277, 283);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(277, 283);
		gl.glVertex2f(283, 297);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(283, 297);
		gl.glVertex2f(275, 287);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(275, 287);
		gl.glVertex2f(281, 306);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(281, 306);
		gl.glVertex2f(282, 315);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(282, 315);
		gl.glVertex2f(283, 330);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(283, 330);
		gl.glVertex2f(274, 325);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(274, 325);
		gl.glVertex2f(269, 325);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(269, 325);
		gl.glVertex2f(266, 328);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(266, 328);
		gl.glVertex2f(261, 331);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(261, 331);
		gl.glVertex2f(257, 334);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(257, 334);
		gl.glVertex2f(258, 353);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(258, 353);
		gl.glVertex2f(261, 376);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(261, 376);
		gl.glVertex2f(260, 380);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(260, 380);
		gl.glVertex2f(257, 378);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(257, 378);
		gl.glVertex2f(252, 357);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(252, 357);
		gl.glVertex2f(252, 349);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(252, 349);
		gl.glVertex2f(249, 345);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(262, 338);
		gl.glVertex2f(262, 338);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(263, 338);
		gl.glVertex2f(271, 340);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(271, 340);
		gl.glVertex2f(281, 338);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(281, 338);
		gl.glVertex2f(287, 336);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(287, 336);
		gl.glVertex2f(296, 333);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(296, 333);
		gl.glVertex2f(305, 333);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(305, 333);
		gl.glVertex2f(314, 334);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(314, 334);
		gl.glVertex2f(320, 336);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(320, 336);
		gl.glVertex2f(327, 341);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(327, 341);
		gl.glVertex2f(336, 350);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(336, 350);
		gl.glVertex2f(339, 359);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(339, 359);
		gl.glVertex2f(340, 368);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(340, 368);
		gl.glVertex2f(343, 381);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(343, 381);
		gl.glVertex2f(340, 388);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(340, 425);
		gl.glVertex2f(348, 418);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(348, 418);
		gl.glVertex2f(353, 410);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(353, 410);
		gl.glVertex2f(356, 407);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(356, 407);
		gl.glVertex2f(355, 394);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(355, 394);
		gl.glVertex2f(356, 390);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(356, 390);
		gl.glVertex2f(356, 382);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(356, 382);
		gl.glVertex2f(358, 368);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(358, 368);
		gl.glVertex2f(352, 357);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(352, 357);
		gl.glVertex2f(343, 350);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(343, 350);
		gl.glVertex2f(337, 348);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(337, 348);
		gl.glVertex2f(329, 345);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(346, 350);
		gl.glVertex2f(350, 344);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(350, 344);
		gl.glVertex2f(352, 341);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(352, 341);
		gl.glVertex2f(358, 338);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(358, 338);
		gl.glVertex2f(363, 331);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(363, 331);
		gl.glVertex2f(364, 324);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(364, 324);
		gl.glVertex2f(362, 319);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(362, 319);
		gl.glVertex2f(361, 316);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(361, 316);
		gl.glVertex2f(359, 314);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(359, 314);
		gl.glVertex2f(356, 308);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(356, 308);
		gl.glVertex2f(354, 308);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(354, 308);
		gl.glVertex2f(353, 306);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(353, 306);
		gl.glVertex2f(356, 303);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(356, 303);
		gl.glVertex2f(356, 298);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(356, 298);
		gl.glVertex2f(355, 297);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(355, 297);
		gl.glVertex2f(353, 297);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(353, 297);
		gl.glVertex2f(350, 296);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(350, 296);
		gl.glVertex2f(346, 297);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(346, 297);
		gl.glVertex2f(341, 298);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(341, 298);
		gl.glVertex2f(339, 301);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(339, 301);
		gl.glVertex2f(336, 303);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(336, 303);
		gl.glVertex2f(335, 305);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(335, 305);
		gl.glVertex2f(334, 307);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(334, 307);
		gl.glVertex2f(334, 303);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(334, 303);
		gl.glVertex2f(334, 300);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(334, 299);
		gl.glVertex2f(331, 299);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(331, 299);
		gl.glVertex2f(328, 302);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(328, 302);
		gl.glVertex2f(326, 303);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(326, 303);
		gl.glVertex2f(323, 307);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(323, 307);
		gl.glVertex2f(320, 308);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(320, 308);
		gl.glVertex2f(320, 311);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(320, 311);
		gl.glVertex2f(320, 318);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(320, 318);
		gl.glVertex2f(322, 323);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(322, 323);
		gl.glVertex2f(323, 331);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(323, 331);
		gl.glVertex2f(322, 336);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(334, 303);
		gl.glVertex2f(328, 295);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(321, 289);
		gl.glVertex2f(321, 289);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(316, 285);
		gl.glVertex2f(316, 285);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(307, 278);
		gl.glVertex2f(307, 278);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(296, 273);
		gl.glVertex2f(296, 273);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(284, 271);
		gl.glVertex2f(284, 271);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(274, 269);
		gl.glVertex2f(274, 269);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(340, 298);
		gl.glVertex2f(336, 293);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(336, 293);
		gl.glVertex2f(333, 287);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(333, 287);
		gl.glVertex2f(333, 287);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(333, 287);
		gl.glVertex2f(326, 283);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(326, 283);
		gl.glVertex2f(321, 280);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(321, 280);
		gl.glVertex2f(316, 274);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(316, 274);
		gl.glVertex2f(311, 270);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(311, 270);
		gl.glVertex2f(305, 266);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(305, 266);
		gl.glVertex2f(299, 264);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(299, 264);
		gl.glVertex2f(290, 261);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(290, 261);
		gl.glVertex2f(281, 260);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(281, 260);
		gl.glVertex2f(275, 259);
		gl.glEnd();
		
		// white seat right
		gl.glColor3f(0.412f, 0.412f, 0.412f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(445, 449);
		gl.glVertex2f(461, 429);
		gl.glVertex2f(564, 419);
		gl.glVertex2f(646, 419);
		gl.glVertex2f(709, 415);
		gl.glVertex2f(728, 420);
		gl.glVertex2f(708, 401);
		gl.glVertex2f(672, 395);
		gl.glVertex2f(554, 396);
		gl.glVertex2f(488, 396);
		gl.glVertex2f(450, 422);
		gl.glVertex2f(445, 444);
		gl.glVertex2f(445, 451);
		gl.glEnd();
		
		// black seat right
		gl.glColor3f(0, 0, 0);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(734, 430);
		gl.glVertex2f(726, 419);
		gl.glVertex2f(708, 412);
		gl.glVertex2f(686, 416);
		gl.glVertex2f(631, 414);
		gl.glVertex2f(573, 419);
		gl.glVertex2f(523, 422);
		gl.glVertex2f(485, 428);
		gl.glVertex2f(458, 428);
		gl.glVertex2f(447, 447);
		gl.glVertex2f(598, 442);
		gl.glVertex2f(733, 432);
		gl.glEnd();
		
		//white seat left
		gl.glColor3f(0.412f, 0.412f, 0.412f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(71, 430);
		gl.glVertex2f(81, 414);
		gl.glVertex2f(109, 402);
		gl.glVertex2f(137, 401);
		gl.glVertex2f(168, 406);
		gl.glVertex2f(205, 410);
		gl.glVertex2f(265, 406);
		gl.glVertex2f(322, 414);
		gl.glVertex2f(346, 428);
		gl.glVertex2f(349, 449);
		gl.glVertex2f(226, 442);
		gl.glVertex2f(133, 433);
		gl.glVertex2f(72, 433);
		gl.glVertex2f(68, 427);
		gl.glEnd();
		
		// black seat right
		gl.glColor3f(0, 0, 0);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(69, 432);
		gl.glVertex2f(75, 421);
		gl.glVertex2f(95, 415);
		gl.glVertex2f(138, 423);
		gl.glVertex2f(201, 421);
		gl.glVertex2f(252, 423);
		gl.glVertex2f(319, 432);
		gl.glVertex2f(342, 439);
		gl.glVertex2f(348, 451);
		gl.glVertex2f(193, 441);
		gl.glVertex2f(91, 432);
		gl.glVertex2f(71, 436);
		gl.glEnd();
		
		// seat lines
		gl.glColor3f(0, 0, 0);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(481, 302);
		gl.glVertex2f(481, 329);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(482, 341);
		gl.glVertex2f(483, 307);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(611, 317);
		gl.glVertex2f(597, 316);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(597, 316);
		gl.glVertex2f(568, 310);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(568, 310);
		gl.glVertex2f(539, 305);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(539, 305);
		gl.glVertex2f(512, 305);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(512, 305);
		gl.glVertex2f(484, 307);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(482, 338);
		gl.glVertex2f(507, 340);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(507, 340);
		gl.glVertex2f(527, 341);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(527, 341);
		gl.glVertex2f(553, 344);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(553, 344);
		gl.glVertex2f(581, 344);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(581, 344);
		gl.glVertex2f(601, 343);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(601, 343);
		gl.glVertex2f(614, 341);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(614, 341);
		gl.glVertex2f(616, 336);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(616, 336);
		gl.glVertex2f(614, 328);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(614, 328);
		gl.glVertex2f(610, 319);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(610, 319);
		gl.glVertex2f(607, 315);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(538, 310);
		gl.glVertex2f(539, 317);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(539, 317);
		gl.glVertex2f(543, 318);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(543, 318);
		gl.glVertex2f(547, 312);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(350, 451);
		gl.glVertex2f(348, 444);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(348, 444);
		gl.glVertex2f(347, 434);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(347, 434);
		gl.glVertex2f(344, 429);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(344, 429);
		gl.glVertex2f(336, 424);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(336, 424);
		gl.glVertex2f(326, 417);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(326, 417);
		gl.glVertex2f(313, 414);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(313, 414);
		gl.glVertex2f(294, 411);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(294, 411);
		gl.glVertex2f(279, 408);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(279, 408);
		gl.glVertex2f(268, 406);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(268, 406);
		gl.glVertex2f(256, 407);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(256, 407);
		gl.glVertex2f(240, 409);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(240, 409);
		gl.glVertex2f(221, 409);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(221, 409);
		gl.glVertex2f(200, 405);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(200, 405);
		gl.glVertex2f(175, 405);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(175, 405);
		gl.glVertex2f(160, 405);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(160, 405);
		gl.glVertex2f(144, 405);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(144, 405);
		gl.glVertex2f(125, 405);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(125, 405);
		gl.glVertex2f(106, 404);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(106, 404);
		gl.glVertex2f(94, 409);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(94, 409);
		gl.glVertex2f(84, 412);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(84, 412);
		gl.glVertex2f(77, 421);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(76, 422);
		gl.glVertex2f(78, 426);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(447, 445);
		gl.glVertex2f(452, 433);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(452, 433);
		gl.glVertex2f(454, 427);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(454, 427);
		gl.glVertex2f(461, 411);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(461, 411);
		gl.glVertex2f(476, 404);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(476, 404);
		gl.glVertex2f(492, 400);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(492, 400);
		gl.glVertex2f(517, 398);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(517, 398);
		gl.glVertex2f(539, 395);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(539, 395);
		gl.glVertex2f(566, 395);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(566, 395);
		gl.glVertex2f(566, 395);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(599, 397);
		gl.glVertex2f(618, 399);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(618, 399);
		gl.glVertex2f(638, 397);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(638, 397);
		gl.glVertex2f(654, 399);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(654, 399);
		gl.glVertex2f(688, 403);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(688, 403);
		gl.glVertex2f(705, 406);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(705, 406);
		gl.glVertex2f(715, 409);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(715, 409);
		gl.glVertex2f(728, 421);
		gl.glEnd();
		
		// radio
		gl.glColor3f(0, 0, 0);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(447, 252);
		gl.glVertex2f(374, 252);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(374, 252);
		gl.glVertex2f(419, 244);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(419, 244);
		gl.glVertex2f(449, 252);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(630, 271);
		gl.glVertex2f(606, 271);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(606, 271);
		gl.glVertex2f(576, 268);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(576, 268);
		gl.glVertex2f(546, 263);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(546, 263);
		gl.glVertex2f(508, 262);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(508, 262);
		gl.glVertex2f(476, 261);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(476, 261);
		gl.glVertex2f(438, 258);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(438, 258);
		gl.glVertex2f(398, 260);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(398, 260);
		gl.glVertex2f(361, 258);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(361, 258);
		gl.glVertex2f(321, 258);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(321, 258);
		gl.glVertex2f(300, 257);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(300, 257);
		gl.glVertex2f(286, 262);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(634, 272);
		gl.glVertex2f(644, 279);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(644, 279);
		gl.glVertex2f(642, 289);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(642, 289);
		gl.glVertex2f(611, 286);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(611, 286);
		gl.glVertex2f(572, 281);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(572, 281);
		gl.glVertex2f(531, 275);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(531, 275);
		gl.glVertex2f(499, 274);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(499, 274);
		gl.glVertex2f(470, 273);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(469, 272);
		gl.glVertex2f(436, 268);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(435, 269);
		gl.glVertex2f(403, 270);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(403, 270);
		gl.glVertex2f(374, 269);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(374, 269);
		gl.glVertex2f(335, 271);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(335, 271);
		gl.glVertex2f(314, 269);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(615, 352);
		gl.glVertex2f(478, 347);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(608, 360);
		gl.glVertex2f(482, 358);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(435, 316);
		gl.glVertex2f(435, 335);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(435, 335);
		gl.glVertex2f(368, 333);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(362, 342);
		gl.glVertex2f(359, 354);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(363, 358);
		gl.glVertex2f(425, 361);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(392, 346);
		gl.glVertex2f(409, 347);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(371, 349);
		gl.glVertex2f(374, 351);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(374, 351);
		gl.glVertex2f(377, 350);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(377, 350);
		gl.glVertex2f(378, 347);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(378, 347);
		gl.glVertex2f(377, 344);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(377, 344);
		gl.glVertex2f(376, 344);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(376, 344);
		gl.glVertex2f(374, 344);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(374, 344);
		gl.glVertex2f(372, 345);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(372, 345);
		gl.glVertex2f(372, 347);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(372, 347);
		gl.glVertex2f(374, 349);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(428, 343);
		gl.glVertex2f(423, 343);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(423, 343);
		gl.glVertex2f(422, 344);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(422, 344);
		gl.glVertex2f(421, 348);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(421, 348);
		gl.glVertex2f(424, 350);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(424, 350);
		gl.glVertex2f(426, 352);
		gl.glEnd();

		// sword
		gl.glColor3f(0, 0, 0);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(460, 274);
		gl.glVertex2f(457, 276);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(457, 276);
		gl.glVertex2f(455, 280);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(455, 280);
		gl.glVertex2f(454, 286);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(454, 286);
		gl.glVertex2f(454, 291);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(454, 291);
		gl.glVertex2f(453, 298);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(453, 298);
		gl.glVertex2f(452, 304);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(452, 304);
		gl.glVertex2f(451, 318);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(451, 318);
		gl.glVertex2f(450, 324);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(450, 324);
		gl.glVertex2f(450, 335);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(450, 335);
		gl.glVertex2f(450, 346);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(450, 346);
		gl.glVertex2f(450, 355);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(459, 275);
		gl.glVertex2f(464, 282);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(465, 294);
		gl.glVertex2f(463, 287);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(464, 294);
		gl.glVertex2f(463, 302);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(463, 302);
		gl.glVertex2f(463, 311);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(463, 311);
		gl.glVertex2f(462, 319);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(462, 319);
		gl.glVertex2f(462, 328);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(462, 328);
		gl.glVertex2f(462, 337);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(462, 337);
		gl.glVertex2f(461, 346);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(461, 346);
		gl.glVertex2f(460, 350);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(460, 350);
		gl.glVertex2f(458, 356);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(450, 340);
		gl.glVertex2f(446, 340);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(446, 340);
		gl.glVertex2f(442, 343);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(442, 343);
		gl.glVertex2f(439, 345);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(439, 345);
		gl.glVertex2f(437, 349);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(437, 349);
		gl.glVertex2f(435, 353);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(435, 353);
		gl.glVertex2f(435, 358);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(435, 358);
		gl.glVertex2f(435, 363);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(435, 363);
		gl.glVertex2f(438, 365);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(438, 365);
		gl.glVertex2f(441, 367);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(441, 367);
		gl.glVertex2f(445, 370);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(445, 370);
		gl.glVertex2f(447, 371);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(458, 372);
		gl.glVertex2f(463, 370);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(463, 370);
		gl.glVertex2f(470, 367);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(470, 367);
		gl.glVertex2f(471, 361);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(471, 361);
		gl.glVertex2f(472, 355);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(472, 355);
		gl.glVertex2f(472, 351);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(472, 351);
		gl.glVertex2f(469, 348);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(469, 348);
		gl.glVertex2f(467, 345);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(467, 345);
		gl.glVertex2f(463, 342);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(463, 342);
		gl.glVertex2f(461, 341);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(451, 376);
		gl.glVertex2f(451, 381);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(451, 381);
		gl.glVertex2f(450, 388);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(450, 388);
		gl.glVertex2f(449, 395);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(449, 395);
		gl.glVertex2f(449, 406);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(449, 406);
		gl.glVertex2f(448, 411);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(448, 411);
		gl.glVertex2f(448, 417);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(448, 418);
		gl.glVertex2f(448, 421);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(459, 377);
		gl.glVertex2f(459, 384);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(459, 384);
		gl.glVertex2f(458, 393);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(458, 393);
		gl.glVertex2f(458, 399);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(458, 399);
		gl.glVertex2f(457, 404);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(457, 404);
		gl.glVertex2f(456, 409);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(452, 294);
		gl.glVertex2f(454, 296);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(454, 296);
		gl.glVertex2f(457, 298);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(457, 298);
		gl.glVertex2f(460, 298);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(460, 298);
		gl.glVertex2f(462, 295);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(451, 310);
		gl.glVertex2f(452, 312);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(452, 312);
		gl.glVertex2f(454, 312);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(454, 312);
		gl.glVertex2f(457, 312);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(457, 312);
		gl.glVertex2f(459, 311);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(460, 311);
		gl.glVertex2f(461, 310);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(449, 334);
		gl.glVertex2f(450, 335);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(450, 335);
		gl.glVertex2f(452, 338);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(452, 338);
		gl.glVertex2f(453, 338);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(453, 338);
		gl.glVertex2f(455, 338);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(455, 338);
		gl.glVertex2f(457, 338);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(457, 338);
		gl.glVertex2f(459, 336);
		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glVertex2f(459, 336);
		gl.glVertex2f(459, 335);
		gl.glEnd();

		// clear
		gl.glColor3f(0.3f, 0.3f, 0.3f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(240, 196);
		gl.glVertex2f(233, 188);
		gl.glVertex2f(224, 184);
		gl.glVertex2f(212, 181);
		gl.glVertex2f(205, 182);
		gl.glVertex2f(198, 185);
		gl.glVertex2f(188, 190);
		gl.glVertex2f(183, 189);
		gl.glVertex2f(179, 195);
		gl.glVertex2f(173, 200);
		gl.glVertex2f(191, 203);
		gl.glVertex2f(228, 201);
		gl.glVertex2f(238, 199);
		gl.glEnd();
		
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(153, 264);
		gl.glVertex2f(149, 281);
		gl.glVertex2f(150, 292);
		gl.glVertex2f(219, 305);
		gl.glVertex2f(272, 291);
		gl.glVertex2f(275, 281);
		gl.glVertex2f(271, 269);
		gl.glVertex2f(272, 252);
		gl.glVertex2f(189, 250);
		gl.glVertex2f(156, 263);
		gl.glEnd();
	}
}
