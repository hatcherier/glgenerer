package org.graphics;

import org.engine.DrawEngine;
import org.input.KeyInput;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

public class EventListener implements GLEventListener {
	
	public static GL2 gl = null;
	
	@Override
	public void display(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
		gl = drawable.getGL().getGL2();
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
		
		Graphics.art();

		/*
		gl.glColor3f(0, 0, 0);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(105, 32);
		gl.glVertex2f(88, 68);
		gl.glVertex2f(158, 74);
		gl.glVertex2f(147, 33);
		gl.glEnd();
		*/
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
		GL2 gl = drawable.getGL().getGL2();
		gl.glClearColor(0.3f, 0.3f, 0.3f, 0);
		
		DrawEngine.shapeInit();
		KeyInput.printNewShape();
	}

	@Override
	public void reshape(GLAutoDrawable drawable,
			int x, int y, int width, int height) {
		// TODO Auto-generated method stub
		GL2 gl = drawable.getGL().getGL2();
		
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
				
		gl.glOrtho(0, UserView.screenWidth,
				UserView.screenHeight, 0, 0, 1);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		
	}

}
